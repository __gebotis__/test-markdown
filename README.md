[just-url][]
[url-with-short-title][]
[url-with-long-title][]
[long][]
[long-with-title][]


[just-url]: https://example.com
[url-with-short-title]: https://example.com "title"
[url-with-long-title]:
  https://example.com
  "a long, long title. It's really really long. Here have words."
[long]:
  https://example.com/a-long-url/another-segment/yet-another-segment/a-really-long-file-name.php.aspx
[long-with-title]:
  https://example.com/a-long-url/another-segment/yet-another-segment/a-really-long-file-name.php.aspx
  "look a title!"